package main

import (
	"flag"
	"fmt"
	"load-balancer/log"
	"load-balancer/util/safebuffer"
	"load-balancer/worker/markovchain"
	"math"
	"math/rand"
	"net/http"
	"time"
)

var (
	host      string
	port      string
	cores     int
	wait      int
	send_body bool
	logger    *log.Logger
	chain     *markovchain.Chain // To generate random text for the responses
	queue     *safebuffer.Buffer
	stats     chan Request
)

type Request struct {
	writer    http.ResponseWriter
	received  time.Time
	dequeue   time.Time
	completed time.Time
	done      chan bool
}

func init() {
	// Seed the random generator
	rand.Seed(time.Now().UnixNano())

	// Setup argument parsing
	flag.StringVar(&host, "host", "", "Specify worker listening host")
	flag.StringVar(&port, "port", "9000", "Specify worker listening port")
	flag.IntVar(&cores, "cores", 1, "Specify worker parallelism")
	flag.IntVar(&wait, "wait", 100, "How long to wait on average ?")
	flag.BoolVar(&send_body, "with-body", true, "Set to true to send back some random text")

	// Seed the random text generator
	chain = markovchain.NewChain(1)
	chain.Write([]byte("At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."))

	// Initialize the queue
	queue = safebuffer.NewBuffer()

	// Start gathering stats
	stats = make(chan Request, 1000)
	go gather_stats(stats)

	// Start worker goroutines
	for w := 0; w < cores; w++ {
		go work()
	}
}

func gather_stats(requests chan Request) {
	var (
		count                         int64
		total_processing_time         time.Duration
		total_squared_processing_time time.Duration
		total_queue_time              time.Duration
	)

	timer := time.Tick(1 * time.Second)

	for {
		select {
		case request := <-requests:
			count++

			total_queue_time += request.dequeue.Sub(request.received)

			processing_time := time.Since(request.received)
			total_processing_time += processing_time
			total_squared_processing_time += processing_time * processing_time
		case <-timer:
			mean := total_processing_time.Seconds() / float64(count)
			stddev := math.Pow(float64(count)*total_squared_processing_time.Seconds()-math.Pow(total_processing_time.Seconds(), 2), .5)
			mean_queue := total_queue_time.Seconds() / float64(count)

			count = 0
			total_processing_time = 0
			total_squared_processing_time = 0
			total_queue_time = 0

			logger.Vlogln(4, "request_mean:", mean, ", request_stddev:", stddev, ",queue_mean:", mean_queue)
		}
	}
}

func weibull(shape, scale, x float64) float64 {
	return 1 - math.Exp(-1*math.Pow(x/scale, shape))
}

func weibull_sample(x0 float64) int {
	for sample := 0.0; ; sample += 10 {
		x := weibull(1.5, float64(wait), sample)
		if x > x0 {
			return int(sample)
		}
	}
	return 0
}

// Process requests from the queue
func work() {
	for {
		request := queue.Remove().(Request)
		request.dequeue = time.Now()

		load := weibull_sample(rand.Float64())
		delay := time.Duration(load) * time.Millisecond
		if wait != 0 {
			time.Sleep(delay)
		}

		// Generate some random text and write it to the response
		if send_body {
			fmt.Fprintf(request.writer, chain.Generate(load)+"\n")
		}

		request.completed = time.Now()
		stats <- request
		request.done <- true
	}
}

func handle(w http.ResponseWriter, r *http.Request) {
	request := Request{
		writer:   w,
		received: time.Now(),
		done:     make(chan bool),
	}
	queue.Insert(request)
	<-request.done
}

func main() {
	flag.Parse()

	// Make a logger
	logger = log.NewLogger(fmt.Sprintf("[W %s]", port))
	logger.SetVerbosity(6)

	http.HandleFunc("/", handle)
	http.ListenAndServe(host+":"+port, nil)
}
