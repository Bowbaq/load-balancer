package main

import (
	"fmt"
	"load-balancer/util"
	"log"
	"runtime"
	"time"
)

type TestCase struct {
	name string
	test func() time.Time
}

func makeTestCase(name string, workers, delay, clients, requests int) TestCase {
	return TestCase{
		name: name,
		test: func() time.Time {
			w := util.StartWorkers(workers, 9000, 8, delay)
			util.StartLoadBalancer(w)

			defer util.KillLoadBalancer()
			defer util.KillWorkers()

			start := time.Now()
			util.GenerateLoad(clients, requests, "http://127.0.0.1:8000")
			elapsed := time.Since(start)

			log.Println("Requests :", clients*requests, "Elapsed:", elapsed, "Requests/s:", float64(clients*requests)/elapsed.Seconds())

			return start
		},
	}
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	tests := []TestCase{
		makeTestCase("OneClientOneWorker", 1, 10, 1, 100),
		makeTestCase("TenClientsOneWorker", 1, 10, 10, 100),
		makeTestCase("HundredClientsOneWorker", 1, 10, 100, 10),
		makeTestCase("HundredClientsTenWorkers", 10, 10, 100, 100),
		makeTestCase("TwoHundredClientsTenWorkers", 10, 10, 200, 100),
		makeTestCase("FiveHundredClientsTenWorkers", 10, 10, 500, 100),
		makeTestCase("HundredClientsHundredWorkers", 100, 10, 100, 1000),
		makeTestCase("TwoHundredClientsHundredWorkers", 100, 10, 200, 1000),
		makeTestCase("FiveHundredClientsHundredWorkers", 100, 10, 500, 1000),
		makeTestCase("ThousandClientsHundredWorkers", 100, 10, 1000, 1000),
	}

	for _, test := range tests {
		log.Println("Running ", test.name, "... ")
		start := test.test()
		log.Println(time.Since(start))
		fmt.Println()
		time.Sleep(5 * time.Second)
	}
}
