package balancer

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"load-balancer/log"
	"load-balancer/pool"
	"load-balancer/statsd"
	"net"
	"net/http"
	"runtime"
	"strconv"
	"time"
)

type StatUpdate struct {
	request_accepted time.Time
	worker_selected  time.Time
	worker_returned  time.Time
	roundtrip_done   time.Time
	header_done      time.Time
	copy_done        time.Time
	error            bool
}

type Token map[string]*Message

type Message struct {
	WorkerCount int
	Load        int64
	Workers     []string
}

func (m *Message) String() string {
	return fmt.Sprintf("(count:%d, load:%d %v)", m.WorkerCount, m.Load, m.Workers)
}

type LoadBalancer struct {
	id int

	addr string // Address the load balancer is listening on (host:port)

	pool *pool.WorkerPool

	logger *log.Logger
	stats  chan StatUpdate
	load   int64

	stashed []*pool.Worker
}

//NewLoadBalancer simply returns a new Load Balancer with a new empty worker pool and connects to the statsd service
func NewLoadBalancer(id int, host, port, ringport, nextaddr string) *LoadBalancer {
	lb := &LoadBalancer{
		id:      id,
		addr:    host + ":" + port,
		pool:    pool.NewWorkerPool(),
		logger:  log.NewLogger("[LOAD BALANCER " + host + ":" + port + "]"),
		stats:   make(chan StatUpdate, 1000000),
		stashed: make([]*pool.Worker, 0),
	}

	lb.logger.SetVerbosity(2)

	if lb.id == -1 {
		return lb
	}

	// Initialize the ring
	var next, previous *net.TCPConn
	var err error
	if lb.id == 0 {
		next, err = lb.dialNext(nextaddr)
		if err != nil {
			panic(err)
		}

		previous, err = lb.listenPrevious(ringport)
		if err != nil {
			panic(err)
		}
	} else {
		previous, err = lb.listenPrevious(ringport)
		if err != nil {
			panic(err)
		}
		next, err = lb.dialNext(nextaddr)
		if err != nil {
			panic(err)
		}
	}

	go lb.reallocate(next, previous)

	return lb
}

//AddWorker puts a new worker into the priority queue
func (lb *LoadBalancer) AddWorker(url string) {
	lb.pool.AddWorker(url)
}

func (lb *LoadBalancer) Start() error {
	lb.logger.Vlogln(2, "Load balancer started on", lb.addr)

	go lb.monitor()

	http.ListenAndServe(lb.addr, lb)

	return nil
}

func (lb *LoadBalancer) ServeHTTP(respwriter http.ResponseWriter, req *http.Request) {
	stats := StatUpdate{
		request_accepted: time.Now(),
	}

	// Grab a new worker
	worker := lb.pool.NextWorker()
	stats.worker_selected = time.Now()

	// Proxify the request
	req.URL.Scheme = worker.RemoteURL.Scheme
	req.URL.Host = worker.RemoteURL.Host
	req.Host = worker.RemoteURL.Host
	// Make sure we keep the connection to the worker alive
	req.Header.Del("Connection")

	res, err := http.DefaultTransport.RoundTrip(req)
	stats.roundtrip_done = time.Now()
	if err != nil {
		stats.error = true
		lb.stats <- stats

		// Put the worker back in the pool for retry
		lb.pool.ErrorRequest(worker)
		// Send 500
		respwriter.WriteHeader(http.StatusInternalServerError)

		return
	}

	res.Header.Del("Content-Length")
	copyHeader(respwriter.Header(), res.Header)
	respwriter.WriteHeader(res.StatusCode)
	stats.header_done = time.Now()

	io.Copy(respwriter, res.Body)
	res.Body.Close()
	stats.copy_done = time.Now()

	lb.pool.CompletedRequest(worker)
	stats.worker_returned = time.Now()

	lb.stats <- stats
}

//Reallocate broadcasts to the other load balancers and if some threshold is met then it requests a worker from the least
//loaded load balancer, based on http://www.badgerr.co.uk/2011/06/20/golang-away-tcp-chat-server/ 
//TODO: if someone goes down skip over them
func (lb *LoadBalancer) reallocate(next, previous *net.TCPConn) {
	//make our map of things
	var token Token

	if lb.id == 0 {
		token = make(Token)
		lb.updateToken(&token)
		lb.sendToken(next, &token)
	}

	buffer := make([]byte, 1024*10)
	for {
		lb.readToken(previous, buffer, &token)

		lb.updateToken(&token)

		time.Sleep(time.Duration(1000.0/float64(len(token))) * time.Millisecond)

		lb.sendToken(next, &token)
		lb.logger.Vlogln(4, "Sent token", token)
	}
}

func (lb *LoadBalancer) dialNext(addr string) (*net.TCPConn, error) {
	var next *net.TCPConn

	for {
		time.Sleep(1 * time.Second)
		tcpaddr, err := net.ResolveTCPAddr("tcp", addr)
		if err != nil {
			lb.logger.Vlogln(2, "error resolving TCP send address in reallocate", err)
			return nil, err
		}

		next, err = net.DialTCP("tcp", nil, tcpaddr) //TODO left and right??
		if err != nil {
			lb.logger.Vlogln(2, "error dialing TCP in reallocate, retrying::", err)
			continue
		}

		return next, nil
	}

	lb.logger.Vlogln(2, "error, 5 failed dial attempts, killing process")
	return nil, errors.New("Failed to connect to next load balancer after 5 attemps")
}

func (lb *LoadBalancer) listenPrevious(port string) (*net.TCPConn, error) {
	var previous *net.TCPConn
	addr, err := net.ResolveTCPAddr("tcp", ":"+port)
	if err != nil {
		return nil, err
	}

	listener, err := net.ListenTCP("tcp", addr)
	if err != nil {
		lb.logger.Vlogln(2, "error Listening TCP address in reallocate", err)
		return nil, err
	}

	previous, err = listener.AcceptTCP()
	if err != nil {
		lb.logger.Vlogln(2, "error Accepting TCP address in reallocate", err)
		return nil, err
	}

	return previous, nil
}

func (lb *LoadBalancer) readToken(previous *net.TCPConn, buffer []byte, token *Token) error {
	read, err := previous.Read(buffer) //TODO use number read to detect errors
	if err != nil {
		lb.logger.Vlogln(2, "error reading in reallocate", err)
		return err
	}

	err = json.Unmarshal(buffer[0:read], &token)
	if err != nil {
		lb.logger.Vlogln(2, "error unmarshaling in reallocate", err)
		return err
	}

	return nil
}

func (lb *LoadBalancer) sendToken(next *net.TCPConn, token *Token) error {
	bytes, err := json.Marshal(token)

	if err != nil {
		lb.logger.Vlogln(2, "problem marshalling ring token!")
		return err
	}
	_, err = next.Write(bytes) //error check with returned number of bytes written?
	if err != nil {
		lb.logger.Vlogln(2, "error writing to TCP in reallocate", err)
		return err
	}

	return nil
}

func (lb *LoadBalancer) updateToken(token *Token) {
	message, ok := (*token)[strconv.Itoa(lb.id)]
	if !ok {
		message = &Message{
			Workers: make([]string, 0),
		}

		(*token)[strconv.Itoa(lb.id)] = message
	}

	//grab workers in this nodes slot and add them:
	for _, worker := range message.Workers {
		lb.AddWorker(worker)
	}
	message.Workers = make([]string, 0)

	message.Load = lb.load
	message.WorkerCount = lb.pool.Len()

	totalworkers := 0
	max_load := int64(0)
	min_load := lb.load
	var max_load_id string
	for k, v := range *token {
		totalworkers += v.WorkerCount
		if v.Load > max_load {
			max_load = v.Load
			max_load_id = k
		}

		if v.Load < min_load {
			min_load = v.Load
		}
	}

	most_loaded := (*token)[max_load_id]
	lb.logger.Vlogln(4, "Most loaded load balancer :", max_load_id)

	lb.logger.Vlogln(4, "Stashed workers :", len(lb.stashed))
	// See if we have any stashed workers
	for i, worker := range lb.stashed {
		if worker.Pending() == 0 {
			lb.logger.Vlogln(4, "Sending stashed worker :", worker)
			// Remove the worker from the stash
			lb.stashed = append(lb.stashed[:i], lb.stashed[min(i+1, len(lb.stashed)):]...)
			most_loaded.Workers = append(most_loaded.Workers, worker.RemoteURL.String())
			return
		}
	}

	if lb.load > min_load || max_load == 0 {
		lb.logger.Vlogln(4, "Not the least loaded, skipping")
		return
	}

	// Grab a worker
	if float64(lb.pool.Len()) < .25*float64(totalworkers)/float64(len(*token)) {
		lb.logger.Vlogln(3, "Warning, load balancer at lower worker threshold")
		return
	}
	worker := lb.pool.RemoveFirst()
	if worker.Pending() == 0 {
		// All good, we can send it
		most_loaded.Workers = append(most_loaded.Workers, worker.RemoteURL.String())
	} else {
		lb.stashed = append(lb.stashed, worker)
	}
}

func (lb *LoadBalancer) monitor() {
	// Start statsd client
	statsd, err := statsd.Dial("ec2-54-224-189-252.compute-1.amazonaws.com:8125")
	if err != nil {
		panic(err)
	}
	
	statsd.SetPrefix(strconv.Itoa(lb.id))

	var (
		success int64
		error   int64

		select_times    int64
		roundtrip_times int64
		header_times    int64
		body_times      int64
		return_times    int64
	)
	before := time.Now()
	timer := time.Tick(1 * time.Second)

	for {
		select {
		case stats := <-lb.stats:
			if stats.error {
				error++
				continue
			}

			success++

			select_times += stats.worker_selected.Sub(stats.request_accepted).Nanoseconds()
			roundtrip_times += stats.roundtrip_done.Sub(stats.worker_selected).Nanoseconds()
			header_times += stats.header_done.Sub(stats.roundtrip_done).Nanoseconds()
			body_times += stats.copy_done.Sub(stats.header_done).Nanoseconds()
			return_times += stats.worker_returned.Sub(stats.copy_done).Nanoseconds()
		case <-timer:
			// Update stats
			statsd.Gauge("live_goroutines", int64(runtime.NumGoroutine()))
			statsd.Gauge("worker_count", int64(lb.pool.Len()))

			if success != 0 {
			    statsd.Gauge("request_completed", success)
			    statsd.Gauge("request_error", error)
			    
				statsd.Timing("selected_worker", select_times/success)
				statsd.Timing("roundtrip_completed", roundtrip_times/success)
				statsd.Timing("header_copied", header_times/success)
				statsd.Timing("body_copy_completed", body_times/success)
				statsd.Timing("worker_returned", return_times/success)

				lb.load = roundtrip_times / time.Millisecond.Nanoseconds() / int64(lb.pool.Len())

				select_times = 0
				roundtrip_times = 0
				header_times = 0
				body_times = 0
				return_times = 0
			} else {
				lb.load = 0
				
				statsd.Gauge("request_completed", 0)
			    statsd.Gauge("request_error", 0)
			}

			elapsed := time.Since(before)
			before = time.Now()

			lb.logger.Vlogln(2, "Request/s:", float64(success)/elapsed.Seconds(), ", Load:", lb.load)

			success = 0
			error = 0
		}
	}
}

func copyHeader(dst, src http.Header) {
	for k, vv := range src {
		for _, v := range vv {
			dst.Add(k, v)
		}
	}
}

func min(i, j int) int {
	if i < j {
		return i
	}

	return j
}
