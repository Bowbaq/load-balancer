package pool

import (
	"fmt"
	"net/url"
)

var workerid int

type Worker struct {
	RemoteURL *url.URL
	id        int
	pending   int
	error     int
	index     int
}

func (w *Worker) String() string {
	return fmt.Sprintf("(id %d, pending %d, error %d)", w.id, w.pending, w.error)
}

func (w *Worker) Pending() int {
	return w.pending
}

func (w *Worker) Error() int {
	return w.error
}

type Queue interface {
	GetAndUpdateFirst(int) *Worker
	Update(*Worker, int)
	ErrorUpdate(*Worker, int)
	RemoveFirst() *Worker
	Insert(*Worker)
	Outstanding() int
	Len() int
}

type WorkerPool struct {
	workers Queue
}

func NewWorkerPool() *WorkerPool {
	return &WorkerPool{
		workers: NewLockedHeapQueue(),
	}
}

func (wp *WorkerPool) AddWorker(address string) {
	url, _ := url.Parse(address)

	wp.workers.Insert(&Worker{
		id:        workerid,
		RemoteURL: url,
	})

	workerid++
}

func (wp *WorkerPool) RemoveFirst() *Worker {
	return wp.workers.RemoveFirst()
}

func (wp *WorkerPool) NextWorker() *Worker {
	return wp.workers.GetAndUpdateFirst(1)
}

func (wp *WorkerPool) CompletedRequest(worker *Worker) {
	wp.workers.Update(worker, -1)
}

func (wp *WorkerPool) ErrorRequest(worker *Worker) {
	wp.workers.ErrorUpdate(worker, -1)
}

func (wp *WorkerPool) Outstanding() int {
	return wp.workers.Outstanding()
}

func (wp *WorkerPool) Len() int {
	return wp.workers.Len()
}
