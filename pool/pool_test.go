package pool

import (
	"log"
	"testing"
)

func TestAddUpdateFinish(t *testing.T) {
	q := NewLockedHeapQueue()

	for i := 0; i < 5; i++ {
		q.Insert(&Worker{id: i})
	}

	workers := make([]*Worker, 0)
	for i := 0; i < 5; i++ {
		workers = append(workers, q.GetAndUpdateFirst(1))
	}

	for i := 0; i < 55; i++ {
		q.GetAndUpdateFirst(1)
	}

	log.Println(q)

	for _, w := range workers {
		for i := 0; i < 12; i++ {
			q.Update(w, -1)
		}
	}

	log.Println(q)
}
