package pool

import (
	"container/heap"
	"sync"
)

type LockedHeapQueue struct {
	worker_heap
	sync.Mutex
}

func NewLockedHeapQueue() *LockedHeapQueue {
	q := new(LockedHeapQueue)
	heap.Init(q)

	return q
}

func (lhq *LockedHeapQueue) Insert(worker *Worker) {
	lhq.Lock()

	heap.Push(lhq, worker)

	lhq.Unlock()
}

func (lhq *LockedHeapQueue) GetAndUpdateFirst(update int) *Worker {
	lhq.Lock()

	worker := heap.Pop(lhq).(*Worker)
	worker.pending += update
	heap.Push(lhq, worker)

	lhq.Unlock()

	return worker
}

func (lhq *LockedHeapQueue) Update(worker *Worker, update int) {
	lhq.Lock()
	defer lhq.Unlock()

	if worker.index < 0 {
		worker.pending += update
		return
	}

	heap.Remove(lhq, worker.index)
	worker.pending += update
	heap.Push(lhq, worker)
}

func (lhq *LockedHeapQueue) ErrorUpdate(worker *Worker, update int) {
	lhq.Lock()
	defer lhq.Unlock()

	if worker.index < 0 {
		worker.pending += update
		worker.error++
		return
	}

	heap.Remove(lhq, worker.index)
	worker.pending += update
	worker.error++
/*  if worker.error < 3 {*/
		heap.Push(lhq, worker)
/*  } else {
        worker.index = -1 // Invalidate the worker
    }*/
}

func (lhq *LockedHeapQueue) RemoveFirst() *Worker {
	lhq.Lock()
	defer lhq.Unlock()

	worker := heap.Pop(lhq).(*Worker)
	worker.index = -1 // Invalidate the worker

	return worker
}

func (lhq *LockedHeapQueue) Outstanding() int {
	lhq.Lock()
	defer lhq.Unlock()

	return lhq.outstanding()
}

func (lhq *LockedHeapQueue) String() string {
	lhq.Lock()
	defer lhq.Unlock()

	return lhq.toString()
}
