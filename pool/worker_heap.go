package pool

import "strings"

type worker_heap []*Worker

func (pq *worker_heap) Len() int { return len(*pq) }

func (pq *worker_heap) Less(i, j int) bool {
	return (*pq)[i].pending < (*pq)[j].pending
}

func (pq *worker_heap) Swap(i, j int) {
	(*pq)[i], (*pq)[j] = (*pq)[j], (*pq)[i]
	(*pq)[i].index = i
	(*pq)[j].index = j
}

func (pq *worker_heap) Push(x interface{}) {
	n := len(*pq)
	worker := x.(*Worker)
	worker.index = n
	*pq = append(*pq, worker)
}

func (pq *worker_heap) Pop() interface{} {
	n := len(*pq)
	worker := (*pq)[n-1]
	worker.index = -1 // for safety
	*pq = (*pq)[0 : n-1]

	return worker
}

func (pq *worker_heap) toString() string {
	workers := make([]string, 0)
	for _, w := range *pq {
		workers = append(workers, w.String())
	}

	return "[" + strings.Join(workers, ", ") + " ]"
}

func (pq *worker_heap) outstanding() int {
	outstanding := 0
	for _, w := range *pq {
		outstanding += w.pending
	}

	return outstanding
}
