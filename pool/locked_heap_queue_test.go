package pool

import (
	"math/rand"
	"net/url"
	"runtime"
	"testing"
	"time"
)

func init() {
	rand.Seed(time.Now().Unix())
}

func load(h Queue, i int, do, done chan bool, release chan *Worker) {
	<-do
	for j := 0; j < i; j++ {
		release <- h.GetAndUpdateFirst(1)
	}
	done <- true
}

func release(h Queue, do, done, reallydone chan bool, release chan *Worker) {
	<-do
	for {
		select {
		case w := <-release:
			h.Update(w, -1)
		case <-done:
			reallydone <- true
		}
	}
}

func BenchmarkLockedHeapConc(b *testing.B) {
	b.StopTimer()

	runtime.GOMAXPROCS(runtime.NumCPU())

	do := make(chan bool)
	done := make(chan bool)
	reallydone := make(chan bool)
	rel := make(chan *Worker, 1000)

	h := NewLockedHeapQueue()

	url, _ := url.Parse("http://127.0.0.1/")
	for w := 0; w < 30000; w++ {
		h.Insert(&Worker{
			id:        w,
			RemoteURL: url,
		})
	}

	for i := 0; i < runtime.NumCPU()*b.N; i++ {
		go load(h, 10, do, done, rel)
	}
	for i := 0; i < runtime.NumCPU()*b.N; i++ {
		go release(h, do, done, reallydone, rel)
	}

	close(do)

	b.StartTimer()
	for i := 0; i < runtime.NumCPU()*b.N; i++ {
		<-reallydone
	}

	runtime.GOMAXPROCS(1)
}

func BenchmarkChannelHeapConc(b *testing.B) {
	b.StopTimer()

	runtime.GOMAXPROCS(runtime.NumCPU())

	do := make(chan bool)
	done := make(chan bool)
	reallydone := make(chan bool)
	rel := make(chan *Worker, 1000)

	h := NewChannelHeapQueue()

	url, _ := url.Parse("http://127.0.0.1/")
	for w := 0; w < 30000; w++ {
		h.Insert(&Worker{
			id:        w,
			RemoteURL: url,
		})
	}

	for i := 0; i < runtime.NumCPU()*b.N; i++ {
		go load(h, 10, do, done, rel)
	}
	for i := 0; i < runtime.NumCPU()*b.N; i++ {
		go release(h, do, done, reallydone, rel)
	}

	close(do)

	b.StartTimer()
	for i := 0; i < runtime.NumCPU()*b.N; i++ {
		<-reallydone
	}

	runtime.GOMAXPROCS(1)
}
