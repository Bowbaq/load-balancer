import sys
from fabric.api import *
from fabric.tasks import execute
import fabric
import signal
import time

env.skip_bad_hosts = True
env.warn_only = True
fabric.state.output['everything'] = False

hosts = []
load_balancer = "ubuntu@ec2-54-224-189-252.compute-1.amazonaws.com"
env.user = sys.argv[1]

with open(sys.argv[2]) as hostfile:
    for host in hostfile.read().strip().split("\n"):
        if host[0] == '#':
            continue
        hosts.append(host)
        
workers = hosts[0:30]

def kill_workers():
  run("killall worker")
  run("killall load-balancer")

execute(kill_workers, hosts=workers)
