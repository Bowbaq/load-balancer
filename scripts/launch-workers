#!/usr/bin/env python

import time
import argparse
import requests
import subprocess

import fabric
from fabric.api import *
from fabric.tasks import execute

#LOAD_BALANCER = "ubuntu@ec2-54-224-189-252.compute-1.amazonaws.com"
NB_WORKERS = 30 # Don't forget to change it in generate-load
WORKER_URLS = []

def update_code_and_build():
    with cd("go/src/load-balancer/"):
        run("git pull")
        run("./scripts/build")

def launch_workers(delay=100, starting_port=9000, count=100):
    global WORKER_URLS
    
    user = env.user
    host = env.host_string
    print user + "@" + host
    
    subprocess.call('./scripts/worker/killall %s %s' % (user, host), shell=True)
    for current_port in xrange(starting_port, starting_port + count):
        try:
            subprocess.check_call('./scripts/worker/launch %s %s %d %d' % (user, host, current_port, delay), shell=True)
        except:
            return
        WORKER_URLS.append('http://' + host + ':' + str(current_port) + '/')
                   
    
parser = argparse.ArgumentParser(description='Create load-balanced cluster')
parser.add_argument('-w','--workers', required=True, help="The number of workers that should be started", type=int, metavar='<worker-count>')
parser.add_argument('-d','--delay', help="Tweak the base of the Weibull distribution for the workers", type=int, default=100, metavar='<delay>')
parser.add_argument('--hosts', required=True, help="Path to a list of hosts, one per line", metavar='<host-file>')
parser.add_argument('-u','--username', required=True, help="Which user to use to log into the GHC machines", metavar='<user>')

args = parser.parse_args()

# Set some fabric environment variables to quiet things down a bit, and not fail too much
env.skip_bad_hosts = True
env.warn_only = True
fabric.state.output['everything'] = False

# Set the SSH user
env.user = args.username

# Read the list of host
HOSTS = []
with open(args.hosts) as hostfile:
    for host in hostfile.read().strip().split("\n"):
        if host[0] == '#':
            continue
        HOSTS.append(host)

WORKERS = HOSTS[0:NB_WORKERS] # Don't forget to change it in generate-load
    
print "Updating codebase ..."
execute(update_code_and_build, hosts=HOSTS[0:1])
print "Done"

print "Launching workers ..."
execute(launch_workers, hosts=WORKERS, delay=args.delay, count=(args.workers / NB_WORKERS))
time.sleep(5)

print "Checking that workers are alive ..."
up = []
for worker in WORKER_URLS:
    try:
        r = requests.get(worker, timeout=1)
    except:
        print "Skipping worker", worker
        continue
    if r.status_code != 200:
        print "Skipping worker", worker
        continue
    up.append(worker)
    if len(up) % 100 == 0:
        print len(up), "..."
WORKER_URLS = up
print "launched", len(WORKER_URLS), "workers"

print "Updating worker list on load balancer ...",
with open("scripts/lb.workers", "w") as lb_clients:
    lb_clients.write("\n".join(WORKER_URLS))
#local("scp scripts/lb.workers %s:/home/ubuntu/lb.workers" % LOAD_BALANCER)
print "Done"
