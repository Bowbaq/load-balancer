import sys
from fabric.api import *
from fabric.tasks import execute
import fabric
import signal
import time

worker_urls = ''
worker_urls2 = ''
#run some workers
for p in range(9000,9010):
  local("./worker/worker --port %d --wait 100 & " % (p))
  worker_urls += 'http://localhost:' + str(p) + '/\n'

#print worker_urls

# Write the worker list to a local file
with open("lb.clients", "w") as lb_clients:
    lb_clients.write(worker_urls.strip())
#run load balancer
local("./load-balancer --port 8000 --workers lb.clients & ")

for p in range(9011,9020):
  local("./worker/worker --port %d --wait 100 & " % (p))
  worker_urls2 += 'http://localhost:' + str(p) + '/\n'

#print worker_urls2

# Write the worker list to a local file
with open("lb2.clients", "w") as lb_clients2:
    lb_clients2.write(worker_urls2.strip())
local("./load-balancer --port 8001 --workers lb2.clients &")

local("./load-balancer --port 8002 --workers lb.clients ")

#run client workload
#local("go run scripts/generate_load.go --clients 100 --duration 20 --target http://localhost:8000")

local("killall worker")
local("killall load-balancer")
