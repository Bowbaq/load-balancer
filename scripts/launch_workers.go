package main

import (
	"flag"
	"fmt"
	"load-balancer/util"
	"strings"
)

var (
	port  int
	count int
	delay int
	cores int
)

func init() {
	// Setup argument parsing
	flag.IntVar(&count, "count", 1, "How many workers")
	flag.IntVar(&delay, "delay", 10, "How long do they wait")
	flag.IntVar(&port, "port", 8000, "Starting port")
	flag.IntVar(&cores, "cores", 2, "How many cores")
}

func main() {
	flag.Parse()

	util.KillWorkers()
	w := util.StartWorkers(count, port, cores, delay)
	fmt.Println(strings.Join(w, "\n"))
}
