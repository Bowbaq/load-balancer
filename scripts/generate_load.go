package main

import (
	"flag"
	"load-balancer/util"
	"log"
	"runtime"
	"time"
)

var (
	clients  int
	duration int
	target   string
)

func init() {
	// Setup argument parsing
	flag.IntVar(&clients, "clients", 1, "How many clients")
	flag.IntVar(&duration, "duration", 60, "For how long")
	flag.StringVar(&target, "target", "http://127.0.0.1:8000/", "Where to direct the load")
}

func main() {
	flag.Parse()

	runtime.GOMAXPROCS(runtime.NumCPU())

	start := time.Now()
	requests := util.GenerateLoad(clients, time.After(time.Duration(duration)*time.Second), target)
	elapsed := time.Since(start)

	defer log.Println("Requests :", requests, "Elapsed:", elapsed, "Requests/s:", float64(requests)/elapsed.Seconds())
}
