package log

import (
	"log"
	"os"
)

type Logger struct {
	*log.Logger
	Verbosity int
}

func NewLogger(prefix string) *Logger {
	return &Logger{
		log.Logger: log.New(os.Stderr, prefix+" ", log.LstdFlags),
		Verbosity:  3,
	}
}

func (log *Logger) SetVerbosity(verbosity int) {
	log.Verbosity = verbosity
}

func (log *Logger) Vlogln(v int, args ...interface{}) {
	if v <= log.Verbosity {
		log.Println(args...)
	}
}

func Println(args ...interface{}) {
	log.Println(args...)
}
