// Unbounded buffer, where underlying values are arbitrary values

package safebuffer

import (
	"errors"
)

// Linked list element
type Node struct {
	val  interface{}
	next *Node
}

type List struct {
	head *Node // Oldest element
	tail *Node // Most recently inserted element
}

func NewList() *List {
	return new(List)
}

func (bp *List) Insert(val interface{}) {
	ele := &Node{val: val}
	if bp.head == nil {
		// Inserting into empty list
		bp.head = ele
	} else {
		bp.tail.next = ele
	}
	bp.tail = ele
}

func (bp *List) Front() (interface{}, error) {
	if bp.head == nil {
		return nil, errors.New("Empty Listfer")
	}
	return bp.head.val, nil
}

func (bp *List) Remove() (interface{}, error) {
	e := bp.head
	if e == nil {
		err := errors.New("Empty Listfer")
		return nil, err
	}
	bp.head = e.next
	// List becoming empty 
	if e == bp.tail {
		bp.tail = nil
	}
	return e.val, nil
}

func (bp *List) Empty() bool {
	return bp.head == nil
}

func (bp *List) Flush() {
	bp.head = nil
	bp.tail = nil
}
