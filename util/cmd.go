package util

import (
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

func kill(what string) {
	cmd := exec.Command("killall", what)
	cmd.Run()
}

func KillWorkers() {
	kill("worker")
}

func KillLoadBalancer() {
	kill("load-balancer")
}

func StartWorkers(n, port, cores, delay int) []string {
	workers := make([]string, 0)
	hostname, _ := os.Hostname()
	log.Println("Launching", n, "workers with", cores, "cores, delay of", delay)
	for p := port; p < port+n; p++ {
		cmd := exec.Command("nohup ./worker/worker", "--host", "", "--port", strconv.Itoa(p), "--cores", strconv.Itoa(cores), "--wait", strconv.Itoa(delay), "2>&1 &> /dev/null < /dev/null & sleep 1; exit 0")
		err := cmd.Run()
		if err != nil {
			log.Println(err)
			n++
			continue
		}
		workers = append(workers, "http://"+hostname+":"+strconv.Itoa(p)+"/")
	}

	time.Sleep(1 * time.Second)

	return workers
}

func StartLoadBalancer(workers []string) {
	cmd := exec.Command("go build")
	cmd.Run()

	cmd = exec.Command("./load-balancer", "--workers", strings.Join(workers, ","), "&")
	cmd.Start()

	time.Sleep(3 * time.Second) // Give the server some time to start
}

func GenerateLoad(clients int, timeout <-chan time.Time, target string) int {
	log.Println("Generating load,", clients, "clients ->", target)
	done := make(chan bool)
	completed := make(chan int)
	for c := 0; c < clients; c++ {
		time.Sleep(100 * time.Millisecond)
		go func() {
			var requests int
			wait_on_error := 1
			for {
				select {
				case <-done:
					completed <- requests
					return
				default:
					resp, err := http.Get(target)
					if err != nil {
						log.Println("GET error:", err)
						wait_on_error *= 2
						if wait_on_error > 500 {
							wait_on_error = 500
						}
						time.Sleep(time.Duration(wait_on_error) * time.Millisecond)
						continue
					}

					_, err = ioutil.ReadAll(resp.Body)
					if err != nil {
						log.Println("READ error:", err)
						resp.Body.Close()
						continue
					}

					resp.Body.Close()
					requests++
				}
			}
		}()
	}

	<-timeout
	var requests int
	for i := 0; i < clients; i++ {
		done <- true
		requests += <-completed
	}

	return requests
}
