package main

import (
	"flag"
	"io/ioutil"
	"load-balancer/balancer"
	_ "net/http/pprof"
	"runtime"
	"strings"
)

var (
	host     string
	port     string
	workers  string
	next     string
	ringport string
	id       int
)

func init() {
	// Setup argument parsing
	flag.StringVar(&host, "host", "", "Specify worker listening host")
	flag.StringVar(&port, "port", "8000", "Specify HTTP listening port")
	flag.StringVar(&workers, "workers", "", "Path to worker file")
	flag.StringVar(&next, "next", "", "Address of the next balancer in the ring")
	flag.StringVar(&ringport, "ringport", "", "Specify ring listening port")
	flag.IntVar(&id, "id", -1, "The id of this machine in the ring")
}

func main() {
	// Make sure we run on all the cores
	runtime.GOMAXPROCS(runtime.NumCPU() * 2)

	// Parse the command line arguments
	flag.Parse()

	if workers == "" {
		panic("Need at least one worker")
	}

	balancer := balancer.NewLoadBalancer(id, host, port, ringport, next)

	content, err := ioutil.ReadFile(workers)
	if err != nil {
		panic("Need at least one worker")
	}
	worker_urls := strings.Split(string(content), "\n")

	for _, url := range worker_urls {
		if url != "" {
			balancer.AddWorker(url)
		}
	}

	err = balancer.Start()
	if err != nil {
		panic(err)
	}
}
